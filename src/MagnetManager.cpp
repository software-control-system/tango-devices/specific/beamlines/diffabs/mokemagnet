// ============================================================================
//
// = CONTEXT
//    TANGO Project - Magnet Manager Class
//
// = FILENAME
//    MagnetManager.cpp
//
// = AUTHOR
//    X. Elattaoui
// ============================================================================

#include <cmath>
#include <yat/utils/XString.h>
#include <yat/time/Timer.h>     //- perf measures
#include <yat/Portability.h>    //- NaN
#include "MagnetManager.hpp"

// ============================================================================
//                          COMMANDS
// ============================================================================
// ----------------------------------------------------------------------------
const std::string CMD_STOP_REGULATION         = "SET_STAND_BY";
const std::string CMD_STOP_REGULATION_RESP    = "REGUL_PAUSED";
// ----------------------------------------------------------------------------
//- POWER SUPPLY ON
const std::string CMD_POWER_SUPPLY_ON         = "SET_POWER_ON";
const std::string CMD_POWER_SUPPLY_ON_RESP    = " POWER_IS_ON";
//- POWER SUPPLY OFF
const std::string CMD_POWER_SUPPLY_OFF        = "SET_POWER_OFF";
const std::string CMD_POWER_SUPPLY_OFF_RESP   = "POWER_IS_OFF";
//- LOCAL
const std::string CMD_LOCAL                   = "SET_LOCAL_MODE";
const std::string CMD_LOCAL_RESP              = "MODE_LOCAL_ACTIF";
//- ERROR
const std::string CMD_CLEAR_ERROR             = "CLEAR_DEFAULT";
// ----------------------------------------------------------------------------
// ============================================================================
//                          MISC
// ============================================================================
const size_t CMD_AND_RESPONSE_LENGTH          = 12;                 //- NOTE : all cmds and replies have the same length (14 characters)
const double MAX_FIELD_VALUE                  = 2.;                 //- 2 TESLA MAX
const double TESLA_TO_GAUSS                   = 10000.;             //- 1T = 10000 Gauss
const double GAUSS_TO_MILLIGAUSS              = 1000.;              //- 1Gauss = 1000 milliGauss
const double AMPERES_TO_MILLIAMPERES          = 1000.;              //-
// ----------------------------------------------------------------------------
//- Possible replies of the controller on error
const std::string POWER_ON_RESP_ON_ERROR      = "DEFAUT_ACTIF";     //- resp when a default is active and power_on cmd is sent
const std::string BAD_FORMATTED_CMD_ERR       = "WRONGCOMMAND";     //- resp on bad formatted cmd or if maintain mode is enabled (it can only be disabled from the front panel)
// ----------------------------------------------------------------------------
const size_t kPERIODIC_TMOUT_MS               = 300;                //- task period = in ms
// ----------------------------------------------------------------------------
//- the YAT user messages
const std::size_t READ_CURRENT       = yat::FIRST_USER_MSG + 1;
const std::size_t WRITE_CURRENT      = yat::FIRST_USER_MSG + 2;
const std::size_t WRITE_RAMP_RATE    = yat::FIRST_USER_MSG + 3;
const std::size_t POWER_SUPPLY_ON    = yat::FIRST_USER_MSG + 10;
const std::size_t POWER_SUPPLY_OFF   = yat::FIRST_USER_MSG + 11;
const std::size_t LOCAL              = yat::FIRST_USER_MSG + 21;
const std::size_t CLEAR_ERROR        = yat::FIRST_USER_MSG + 30;
// ============================================================================

namespace MokeMagnet_ns
{
// ============================================================================
// MagnetManager::MagnetManager
// ============================================================================
MagnetManager::MagnetManager (Tango::DeviceImpl * host_device,
                              std::string communicationLinkName)                 //- to communicate with the supply controller
  : yat4tango::DeviceTask(host_device),
    m_communicationLink(0),
    m_host_dev(host_device),
    m_communicationDeviceName(communicationLinkName),
    m_systemState(Tango::INIT),
    m_systemStatus("Init on the way."),
    m_currentValue(yat::IEEE_NAN),
    m_is_value_reached(false),
    m_proxy_ok(true)
{
  //- Noop
}

// ============================================================================
// MagnetManager::~MagnetManager
// ============================================================================
MagnetManager::~MagnetManager (void)
{
  //- Noop
}

// ============================================================================
// Task and Messages
// ============================================================================
//-----------------------------------------------
//- the user core of the Task -------------------
void MagnetManager::process_message (yat::Message& _msg)
throw (Tango::DevFailed)
{
  //- The DeviceTask's lock_ -------------
  DEBUG_STREAM << "MagnetManager::process_message::receiving msg " << _msg.to_string() << std::endl;

  //- handle msg
  switch (_msg.type())
  {
  //- THREAD_INIT =======================
  case yat::TASK_INIT:
    {
      try
      {
        create_proxy();
        //- configure thread
        enable_timeout_msg(false);
        //- thread periodic message period in ms
        set_periodic_msg_period(kPERIODIC_TMOUT_MS);
        //- enable periodic at INIT : update current and field values from controller
        enable_periodic_msg(true);
      }
      catch(Tango::DevFailed& df)
      {
        FATAL_STREAM << "System initialization FAILED : caught a DEVFAILED exception." << df << std::endl;
        throw;
      }
      catch(...)
      {
        FATAL_STREAM << "System initialization FAILED : caught a UNKNOWN exception." << std::endl;
        throw;
      }
    }
    break;
  //- TASK_EXIT =======================
  case yat::TASK_EXIT:
    {
      //- "release" code goes here
      try
      {
        if (m_proxy_ok)
        {
          //- the current in the controller must be NULL !
          write_current(0.);

          //- send power_off cmd when the current is 0.
          while (m_systemState == Tango::MOVING)
          {
            update_system_state_status();
          }
        }//-if proxy

        delete_proxy();

        DEBUG_STREAM << "MagnetManager::handle_message handling TASK_EXIT thread is quitting ..." << std::endl;
      }
      catch(Tango::DevFailed& df)
      {
        FATAL_STREAM << "System exit on error -> caught a DEVFAILED exception." << df << std::endl;
      }
      catch(...)
      {
        FATAL_STREAM << "System exit on error  -> caught a UNKNOWN exception!" << std::endl;
      }
    }
    break;
  //- TASK_PERIODIC ===================
  case yat::TASK_PERIODIC:
    {
      DEBUG_STREAM << "MagnetManager::handle_message handling TASK_PERIODIC msg ..." << std::endl;
      //- code relative to the task's periodic job goes here
      //- PERIODIC used to manage the magnet switch and to update system data and state/status
      try
      {
        yat::Timer t;

        do_periodic_job();
        m_nb_retries++;
      }
      catch(Tango::DevFailed& df)
      {
        ERROR_STREAM << "Periodic msg on error -> caught a DEVFAILED exception." << df << std::endl;
      }
      catch(...)
      {
        ERROR_STREAM << "Periodic msg on error -> caught a UNKNOWN exception." << std::endl;
      }
    }
    break;
  //- READ_CURRENT =======================
  case READ_CURRENT:
    {
      read_current();
    }
    break;
  //- WRITE_CURRENT =======================
  case WRITE_CURRENT:
    {
      double value = _msg.get_data<double>();

      //- send
      write_current(value);
    }
    break;
  //- WRITE_RAMP_RATE =======================
  case WRITE_RAMP_RATE:
    {
      double value = _msg.get_data<double>();

      //- send
      write_rampRate(value);
    }
    break;
  //- POWER_SUPPLY_ON =======================
  case POWER_SUPPLY_ON:
    {
      power_on_i();
    }
    break;
  //- POWER_SUPPLY_OFF =======================
  case POWER_SUPPLY_OFF:
    {
      power_off_i();
    }
    break;
  //- LOCAL =======================
  case LOCAL:
    {
      local_i();
    }
    break;
  //- CLEAR_ERROR =======================
  case CLEAR_ERROR:
    {
      clear_error_i();
    }
    break;
  } //- switch (_msg.type())
} //- MagnetManager::handle_message

// ============================================================================
// MagnetManager::initialise_system
// ============================================================================
void MagnetManager::initialise_system (void)
{
  //- send power on command
  try
  {
    //this->power_on_i();
  }
  catch(...)
  {
    throw;
  }
}

// ============================================================================
//                      - CURRENT MANAGEMENT -
// ============================================================================

//- coefficients � utiliser pour une valeur de courant en Ampere (valeur de champ calculee en TESLA)
//- formule : A + B1*X + B2*X� + B3*x3
//- A = 0, B1 = 0.0066, B2 = -1.9926E-7, B3 = -6.3335E-9

// ============================================================================
// MagnetManager::set_currentValue
// ============================================================================
void MagnetManager::set_currentValue(double val)
{
  {
    yat::AutoMutex<> guard(m_state_status_mutex);
    //- for ScanServer : set state MOVING immediately !! Then, the state will be updated in the PERIODIC msg.
    m_systemState  = Tango::MOVING;
  }
  //- prepare msg
  yat::Message* msg = 0;

  msg = new yat::Message(WRITE_CURRENT, MAX_USER_PRIORITY);

  if ( !msg )
  {
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "MagnetManager::set_current");
  }

  //- attach data
  msg->attach_data(val);

  //- don't wait till the message is processed !!
  post(msg);
}

// ============================================================================
// MagnetManager::write_current
// ============================================================================
void MagnetManager::write_current(double val)
{

  //- TODO : check Min/Max limits !
  m_curr_set_point_value = val;

  //- 1) convert it in milliAmp
//  double val = value * AMPERES_TO_MILLIAMPERES;
  //- convert it to microAmp as expected
//  val /= MICRO_AMP_TO_AMP;

  //- extract entire part (and delete decimal part)
  //val = ::floor(val);

  //- 2) convert the value to string
  std::string valStr = yat::XString<double>::to_string(val);

//  std::size_t idx = valStr.find(".");
//  if ( idx != std::string::npos )
//    valStr.erase(idx);
//  std::cout << "\n\n\tMagnetManager::write_current -> erase = $"  << valStr << "$" << std::endl;

  //- 3) for this cmd, the value must be on 9 char
//  while ( valStr.size() < 9 )
//  {
  //- fill with 0
//    valStr = "0" + valStr;
//  }

  std::stringstream cmd_to_send;
  std::string cmd("");
  std::string sign("");

  //- add sign
  if (val >= 0 )
    sign = "+";

  //- prepare the cmd
  cmd_to_send  << "SET_CURRENT " << sign << valStr << std::ends;
  cmd = cmd_to_send.str();

  std::string response("");
  try
  {
    //- send the command
    response = write_read( cmd );
  }
  catch(...)
  {
    return;
  }

  m_is_value_reached = false;
  m_nb_retries = 0;
}

// ============================================================================
// MagnetManager::get_currentValue -> get magnet current
// ============================================================================
double MagnetManager::get_currentValue()
{
  //- update in critical section !!
  yat::AutoMutex<> guard(m_current_value_mutex);
  return m_currentValue;
}

// ============================================================================
// MagnetManager::read_current
// ============================================================================
void MagnetManager::read_current()
{
  static std::string cmd_to_send("GET_CURRENT"); //- cmd
  static std::string begPattern("=");             //- response begins with
  static std::string endPattern("A");            //- response ends with
  double val = yat::IEEE_NAN;                     //- the extracted value
  std::string response("");

  try
  {
    response = write_read( cmd_to_send );
  }
  catch(...)
  {
    return;
  }

  //- response is in the form "CsxxxxxxxxuA" and the unit is in microAmp !
  std::size_t begIdx = 0;                //- start index to extract the value in the response
  std::size_t endIdx = 0;                //- end index to extract the value in the response
  if ( (begIdx = response.find(begPattern)) != std::string::npos)
  {
    begIdx += begPattern.size();
  }
  else
    begIdx = 0;

  if ( (endIdx = response.find(endPattern)) == std::string::npos)
  {
    //- no delimiter found : no data to extract
    endIdx = begIdx;
  }

  std::string valStr("");
  valStr = response.substr(begIdx, (endIdx - begIdx));

  if ( !response.empty() )
  {
    //- convert �A received value in ampere
    val = yat::XString<double>::to_num(valStr); // * MICRO_AMP_TO_AMP;
  }

  {
    //- update in critical section !!
    yat::AutoMutex<> guard(m_current_value_mutex);
    m_currentValue = val;
  }
}

// ============================================================================
// MagnetManager::set_rampRate
// ============================================================================
void MagnetManager::set_rampRate(double val)
{
  {
    yat::AutoMutex<> guard(m_state_status_mutex);
    //- for ScanServer : set state MOVING immediately !! Then, the state will be updated in the PERIODIC msg.
    if ( m_systemState == Tango::MOVING )
    {
      Tango::Except::throw_exception ("OPERATION_NOT_ALLOWED",
                                      "Stop ramp before any change!",
                                      "MagnetManager::set_rampRate");
    }
  }
  //- prepare msg
  yat::Message* msg = 0;

  msg = new yat::Message(WRITE_RAMP_RATE, MAX_USER_PRIORITY);

  if ( !msg )
  {
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "MagnetManager::set_rampRate");
  }

  //- attach data
  msg->attach_data(val);

  //- don't wait till the message is processed !!
  post(msg);
}

// ============================================================================
// MagnetManager::write_rampRate
// ============================================================================
void MagnetManager::write_rampRate(double val)
{
  std::stringstream cmd_to_send;
  std::string cmd("");
  std::string sign("");

  //- convert the value to string
  std::string valStr = yat::XString<double>::to_string(val);

  //- prepare the cmd
  cmd_to_send  << "SET_CURRENT_SPEED " << valStr << std::ends;

  cmd = cmd_to_send.str();

  std::string response("");
  try
  {
    //- send the command
    response = write_read( cmd );
  }
  catch(...)
  {
    return;
  }
}

// ============================================================================
// MagnetManager::get_ramp_rate -> get magnet current ramp rate
// ============================================================================
double MagnetManager::get_ramp_rate()
{
  //- update in critical section !!
  yat::AutoMutex<> guard(m_current_ramprate_mutex);
  return m_ramp_rate;
}

// ============================================================================
// MagnetManager::read_ramp_rate
// ============================================================================
void MagnetManager::read_ramp_rate()
{
  static std::string cmd_to_send("GET_CURRENT_SPEED"); //- cmd
  static std::string begPattern("=");                  //- response begins with
  static std::string endPattern("A");                  //- response ends with
  double val = yat::IEEE_NAN;                          //- the extracted value
  std::string response("");

  try
  {
    response = write_read( cmd_to_send );
  }
  catch(...)
  {
    return;
  }

  //- response is in the form "CURRENT_SPEED = xxx A/s\n\r" and the unit is in A/sec!
  size_t begIdx = 0;                //- start index to extract the value in the response
  size_t endIdx = 0;                //- end index to extract the value in the response
  if ( (begIdx = response.find(begPattern)) != std::string::npos)
  {
    begIdx += begPattern.size();
  }
  else
    begIdx = 0;

  if ( (endIdx = response.find(endPattern)) == std::string::npos)
  {
    //- no delimiter found : no data to extract
    endIdx = begIdx;
  }

  std::string valStr("");
  valStr = response.substr(begIdx, (endIdx - begIdx));

  if ( !response.empty() )
  {
    //- convert the received value in ampere
    val = yat::XString<double>::to_num(valStr);
  }

  {
    //- update in critical section !!
    yat::AutoMutex<> guard(m_current_ramprate_mutex);
    m_ramp_rate = val;
  }
}

// ============================================================================
//                            - COMMANDS -
// ============================================================================

// ============================================================================
// MagnetManager::power_on -> power supply ON
// ============================================================================
void MagnetManager::power_on()
{
  //- prepare msg
  yat::Message* msg = new yat::Message(POWER_SUPPLY_ON, MAX_USER_PRIORITY);

  if ( !msg )
  {
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "MagnetManager::power_on");
  }

  //- don't wait till the message is processed !!
  post(msg);
}

// ============================================================================
// MagnetManager::power_on_i
// ============================================================================
void MagnetManager::power_on_i()
{
  //- prepare cmd and response
  static std::string cmd_to_send(CMD_POWER_SUPPLY_ON);  //- cmd
  bool check_length = false;

  std::string response = write_read( cmd_to_send, check_length );
}

// ============================================================================
// MagnetManager::power_off -> power supply OFF
// ============================================================================
void MagnetManager::power_off()
{
  //- prepare msg
  yat::Message* msg = new yat::Message(POWER_SUPPLY_OFF, MAX_USER_PRIORITY);

  if ( !msg )
  {
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "MagnetManager::power_off");
  }

  //- don't wait till the message is processed !!
  post(msg);
}

// ============================================================================
// MagnetManager::power_off_i
// ============================================================================
void MagnetManager::power_off_i()
{
  //- prepare cmd and response
  static std::string cmd_to_send(CMD_POWER_SUPPLY_OFF);
  bool check_length = false;

  std::string response = write_read( cmd_to_send, check_length );
}

// ============================================================================
// MagnetManager::local
// ============================================================================
void MagnetManager::local()
{
  //- prepare msg
  yat::Message* msg = new yat::Message(LOCAL, MAX_USER_PRIORITY);

  if ( !msg )
  {
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "MagnetManager::local");
  }

  //- don't wait till the message is processed !!
  post(msg);
}

// ============================================================================
// MagnetManager::local_i
// ============================================================================
void MagnetManager::local_i()
{
  //- prepare cmd and response
  static std::string cmd_to_send(CMD_LOCAL);
  bool check_length = false;

  std::string response = write_read( cmd_to_send, check_length );
}

// ============================================================================
// MagnetManager::clear_error
// ============================================================================
void MagnetManager::clear_error()
{
  //- prepare msg
  yat::Message* msg = new yat::Message(CLEAR_ERROR, MAX_USER_PRIORITY);

  if ( !msg )
  {
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "MagnetManager::clear_eror");
  }

  //- don't wait till the message is processed !!
  post(msg);
}

// ============================================================================
// MagnetManager::clear_error_i
// ============================================================================
void MagnetManager::clear_error_i()
{
  //- prepare cmd and response
  static std::string cmd_to_send(CMD_CLEAR_ERROR);
  bool check_length = false;

  std::string response = write_read( cmd_to_send, check_length );
}

// ============================================================================
// MagnetManager::update_system_state_status :
// ============================================================================
void MagnetManager::update_system_state_status()
{
  // {
  //   yat::AutoMutex<> guard(m_state_status_mutex);
  //   m_systemStatus = "Device is up and running";
  //   m_systemState  = Tango::ON;
  // }

  //- check proxy is OK
  if ( !m_communicationLink )
  {
    yat::AutoMutex<> guard(m_state_status_mutex);
    m_systemStatus = "Device initialization failed : check device proxy is defined in device property.";
    m_systemState  = Tango::FAULT;
    return; //- give up
  }

  //- check proxy is up
  try
  {
    m_communicationLink->get_device_proxy()->ping();
  }
  catch(Tango::DevFailed& df)
  {
    FATAL_STREAM << df << std::endl;
    yat::AutoMutex<> guard(m_state_status_mutex);
    m_systemStatus = "Communication device is not reachable.";
    m_systemState  = Tango::FAULT;
    m_proxy_ok = false;
    return; //- give up
  }

  //- check proxy is up
  Tango::DevState comState = Tango::UNKNOWN;
  try
  {
    m_communicationLink->command_out("State", comState);
    if ( comState == Tango::ALARM || comState == Tango::FAULT )
    {
      yat::AutoMutex<> guard(m_state_status_mutex);
      m_systemStatus = "Communication device state is FAULT or ALARM.";
      m_systemState  = Tango::FAULT;
      FATAL_STREAM << m_systemStatus << std::endl;
    }
    if ( comState == Tango::CLOSE )
    {
	    m_communicationLink->command("Open");
	  }
  }
  catch(Tango::DevFailed& df)
  {
    FATAL_STREAM << df << std::endl;
    yat::AutoMutex<> guard(m_state_status_mutex);
    m_systemStatus = "Communication device does not answer to a State request.";
    m_systemState  = Tango::FAULT;
    m_proxy_ok = false;
    return; //- give up
  }

  //- check if last cmd resp is on error
  if ( m_last_resp.find(POWER_ON_RESP_ON_ERROR) != std::string::npos )
  {
    yat::AutoMutex<> guard(m_state_status_mutex);
    m_systemStatus = "A default is ACTIVE, try ResetDefault command first.";
    m_systemState  = Tango::FAULT;
    return; //- give up
  }
  //- this case should not happened!
  else if  ( m_last_resp.find(BAD_FORMATTED_CMD_ERR) != std::string::npos )
  {
    yat::AutoMutex<> guard(m_state_status_mutex);
    m_systemStatus = "Controller replies \"WRONGCOMMAND\" : check device status details to see if Maintenance mode is ON.\n If so, disable it from the front panel.";
    m_systemState  = Tango::FAULT;
    return; //- give up
  }
  // else

  //- all is ok, check errors
  {
    static std::string cmd("GET_ERRORS");
    std::string resp("");
    std::size_t errors = 0;
  	
    m_communicationLink->command_inout("WriteRead", cmd, resp);
    errors = yat::XString<double>::to_num(resp);
    
    if ( errors )
    {
	    yat::AutoMutex<> guard(m_state_status_mutex);
	    m_systemState  = Tango::FAULT;
	    m_systemStatus+= "ERRORS:\n";
	    if ( errors == (1 << 0) )
	    	m_systemStatus += "Interlock_1 failure\n";
	    if ( errors == (1 << 1) )
	    	m_systemStatus += "Interlock_2 failure\n";
	    if ( errors == (1 << 2) )
	    	m_systemStatus += "Interlock_3 failure\n";
	    if ( errors == (1 << 3) )
	    	m_systemStatus += "Transistors failure\n";
	    if ( errors == (1 << 4) )
	    	m_systemStatus += "Temperature overheating\n";
	    if ( errors == (1 << 5) )
	    	m_systemStatus += "Hall Probe polarity fault\n";
	    if ( errors == (1 << 6) )
	    	m_systemStatus += "Hall Probe  placement failure\n";
	    if ( errors == (1 << 7) )
	    	m_systemStatus += "Excessive current occured\n";
	    if ( errors == (1 << 8) )
	    	m_systemStatus += "Excessive voltage occured\n";
	    if ( errors == (1 << 9) )
	    	m_systemStatus += "Fault is active\n";
	    if ( errors == (1 << 10) )
	    	m_systemStatus += "Fault occured, saved but not active anymore\n";
    }
  }
}

// ============================================================================
//            - INTERNAL METHODS -
// ============================================================================

// ============================================================================
// MagnetManager::do_periodic_job
// ============================================================================
void MagnetManager::do_periodic_job()
{
  double currDelta  = 0.01;    //- Ampere
  double high_limit = yat::IEEE_NAN;
  double low_limit  = yat::IEEE_NAN;
  double val_to_check = yat::IEEE_NAN;

  try
  {
    //- update state/status when in STANDBY
    if ( m_is_value_reached )
    {
      update_system_state_status();
    }
  }
  catch(...)
  {
    ERROR_STREAM << "MagnetManager::periodic -> update_system_state_status FAILED!" << std::endl;
    //return;
  }

  //- update current value
  read_current();
  //- update current value
  read_ramp_rate();

//  if ( _use_formula )
  {
    high_limit = m_curr_set_point_value + currDelta;
    low_limit  = m_curr_set_point_value - currDelta;
    //- update in critical section !!
    yat::AutoMutex<> guard(m_current_value_mutex);
    val_to_check = m_currentValue;
  }
//   else
//   {
//     high_limit = this->_field_set_point_value + fieldDelta;
//     low_limit  = this->_field_set_point_value - fieldDelta;
//     //- update in critical section !!
//     yat::AutoMutex<> guard(this->_field_value_mutex);
//     val_to_check = this->_fieldProbeValue;
//   }

  //- compare values and user limits
  if ( !m_is_value_reached )
  {
    if ( (val_to_check > low_limit) && (val_to_check < high_limit) )
    {
      m_is_value_reached     = true;
      INFO_STREAM << "\tMagnetManager::do_periodic_job -> _CURRENT_limits DONE."
                  << "\n low_limit = "    << low_limit
                  << "\n val_to_check = " << val_to_check
                  << "\n high_limit = "   << high_limit
                  << std::endl;
      yat::AutoMutex<> guard(m_state_status_mutex);
      m_systemState = Tango::STANDBY;
      m_systemStatus= "Ready";
    }
  }
}

// ============================================================================
// MagnetManager::create_proxy
// ============================================================================
void MagnetManager::create_proxy()
{
  try
  {
    if ( !m_communicationDeviceName.empty() )
    {
      //- create proxy
      m_communicationLink = new Tango::DeviceProxyHelper(m_communicationDeviceName, m_host_dev);

      //- check device is up !
      m_communicationLink->get_device_proxy()->ping();
    }
  }
  catch(...)
  {
    //- delete proxy
    delete_proxy();
  }
}

// ============================================================================
// MagnetManager::delete_proxy
// ============================================================================
void MagnetManager::delete_proxy()
{
  if ( m_communicationLink )
  {
    delete m_communicationLink;
    m_communicationLink = 0;
  }
}

// ============================================================================
// MagnetManager::check_length
// ============================================================================
void MagnetManager::check_length(std::string& str_to_check)
{
  if ( str_to_check.size() != CMD_AND_RESPONSE_LENGTH )
  {
//    std::cout << "\n\nMagnetManager::check_length of *" << str_to_check.c_str()
//              << "* size is : " << str_to_check.size()
//              << "* but expected : " << CMD_AND_RESPONSE_LENGTH
//              << std::endl;
//    Tango::Except::throw_exception("BAD_PARAMETER",
//      str_to_check + std::string(" length is not the expected one."),
//      "MagnetManager::check_length");
  }
}

// ============================================================================
// MagnetManager::write_read
// ============================================================================
std::string MagnetManager::write_read(std::string& cmd, bool check_resp_length)
{
  std::string response("");

  if ( !m_communicationLink )
  {
    return response;
  }

  //- check communication state
  if ( m_communicationLink->get_device_proxy()->state() == Tango::CLOSE )
    return response;

  try
  {
    //- send the cmd and read its response
    m_communicationLink->command_inout("WriteRead", cmd, response);
    //- check response length
    if ( check_resp_length )
      check_length(response);

    //- find last characters (it returns a '\n' and a '\r' at the end of each response)
    std::size_t charToEraseIdx = response.find_first_of("\n\r");

    //- now erase these characters
    response.erase(charToEraseIdx);
  }
  catch(Tango::DevFailed & df)
  {
    std::string error = "MagnetManager::write_read received a DevFailed exception : cmd $" + cmd + "$ & resp $" + response + "\n";
    FATAL_STREAM << error << std::endl;
    FATAL_STREAM << "df :\n" << df << std::endl;
    yat::AutoMutex<> guard(m_state_status_mutex);
    m_last_cmd = cmd;
    m_last_resp= response;
    Tango::Except::re_throw_exception (df,
                                       "COMMUNICATION_ERROR",
                                       error.c_str(),
                                       "MagnetManager::write_read");
  }
  catch(...)
  {
    std::string error = "MagnetManager::write_read received a [...] exception : cmd $" + cmd + "$ & resp $" + response + "\n";
    FATAL_STREAM << error << std::endl;
    yat::AutoMutex<> guard(m_state_status_mutex);
    m_last_cmd = cmd;
    m_last_resp= response;
    Tango::Except::throw_exception ("COMMUNICATION_ERROR",
                                       error.c_str(),
                                       "MagnetManager::write_read");
  }

  return response;
}

} //- end namespace
