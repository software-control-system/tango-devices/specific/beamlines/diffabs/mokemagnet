// ============================================================================
//
// = CONTEXT
//    TANGO Project - Magnet Manager Class
//
// = FILENAME
//    MagnetManager.hpp
//
// = AUTHOR
//    X. Elattaoui
// ============================================================================

#ifndef _MAGNET_MGR_CLASS_H_
#define _MAGNET_MGR_CLASS_H_

#include <tango.h>
#include <string>
#include <DeviceProxyHelper.h>
#include <yat4tango/DeviceTask.h>

/**
 *  \brief Visible class from the DServer
 *
 *  \author Xavier Elattaoui
 *  \date 05-2021
 */
namespace MokeMagnet_ns
{

class MagnetManager : public yat4tango::DeviceTask
{
public:

  /**
  *  \brief Initialization.
  */
  MagnetManager ( Tango::DeviceImpl * host_device,
                  std::string communicationLinkName);

  /**
  *  \brief Release resources.
  */
  virtual ~MagnetManager ();

  //- load magnet type parameters in the supply controller
  //- NOT USED : void configure();

  /**
  *  \brief MagnetManager getters and setters
  */

  double get_currentValue();

  void set_currentValue(double);

  void set_rampRate(double);
  double get_ramp_rate();

  /**
  *  \brief Power supply commands
  */
  void power_on ();
  void power_off();

  // void remote();
  void local ();
  
  void clear_error();

  /**
  *  \brief Returns the composite state of all subsystems (Supply and Switch)
  */
  Tango::DevState get_state_status (std::string& status) {
    yat::AutoMutex<> guard(m_state_status_mutex);
    status = m_systemStatus;
    return m_systemState;
  }


  bool is_periodic_msg_enabled () {
    return this->periodic_msg_enabled();
  }

protected:

  //- internal mutex
  yat::Mutex  m_current_value_mutex;
  yat::Mutex  m_current_ramprate_mutex;
  yat::Mutex  m_state_status_mutex;

  //- process_message (implements yat4tango::DeviceTask pure virtual method)
  virtual void process_message (yat::Message& msg)
  throw (Tango::DevFailed);

private:

  /**
  *  \brief magnet controller initialization
  */
  void initialise_system();

  void do_periodic_job();

  void write_current(double);
  void read_current();

  void write_rampRate(double);
  void read_ramp_rate();

  void power_on_i();
  void power_off_i();

  // void remote_i();
  void local_i ();
  
  void clear_error_i();

  //- update system state/status
  void update_system_state_status();

  //- proxy management
  void create_proxy();
  void delete_proxy();
  void check_length(std::string&);
  std::string write_read(std::string&, bool check_length = false);

  //- proxies
  Tango::DeviceProxyHelper* m_communicationLink;

  //- the host device
  Tango::DeviceImpl * m_host_dev;

  //- device name
  std::string m_communicationDeviceName;

  //- system state/status
  Tango::DevState m_systemState;
  std::string     m_systemStatus;

  //- controller values
  double m_currentValue;
  double m_curr_set_point_value;
  double m_ramp_rate;

  /** \brief
  *
  *  The controller does not send back a state/status to indicate a ramp is on the way :
  *  - the read back value is compared with the user set value to indicate a Tango RUNNING
  *  - or STANDBY state.
  *  - To do this, the periodic message is enabled while the value is not reached and its state is RUNNING !
  */
  bool m_is_value_reached;
  unsigned short m_nb_retries;
  bool m_proxy_ok;
  std::string m_last_cmd;
  std::string m_last_resp;

};

} //- end namespace

#endif // _MAGNET_MGR_CLASS_H_
