from conan import ConanFile

class MokeMagnetRecipe(ConanFile):
    name = "mokemagnet"
    executable = "ds_MokeMagnet"
    version = "1.1.2"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Xavier Elattaoui"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/specific/beamlines/diffabs/mokemagnet.git"
    description = "MokeMagnet device"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*"
    
    def requirements(self):
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
